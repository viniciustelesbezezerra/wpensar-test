class UpdateProduct
  include Sidekiq::Worker

  def perform(param_id, params)
    product = Product.find param_id
    product.update_attributes(params)
  end
end
