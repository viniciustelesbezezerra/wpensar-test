class Creation
  include Sidekiq::Worker

  def perform(product)
    Product.create!(name: product['name'], price: product['price'])
  end
end
